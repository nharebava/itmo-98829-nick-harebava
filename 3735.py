# Принято

# Какой тип данных возвращает input()? Что делает функция str()?
newstring = str(input())
# При использовании именнованных аргументов пробелы вокруг равно не ставятся
# Что в данном случае меняет end?
print(newstring[2], end = '\n')
print(newstring[-2], end = '\n')
print(newstring[0:5], end = '\n')
print(newstring[0:-2], end = '\n')
print(newstring[0:len(newstring):2], end ='\n')
print(newstring[1:len(newstring):2], end = '\n')
print(newstring[::-1], end = '\n')
print(newstring[::-2], end = '\n')
print(len(newstring))
