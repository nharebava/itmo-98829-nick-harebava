from kivy.app import App 
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.slider import Slider
from kivy.uix.gridlayout import GridLayout
from kivy.properties import ObjectProperty
from kivy.core.window import Window
from kivymd.theming import ThemeManager
from kivy.storage.jsonstore import JsonStore
import random

Window.size = (480, 853)


class Conteiner(GridLayout):
    
    my_texinput1= ObjectProperty()
    my_textinput = ObjectProperty()
    store = JsonStore('hello.json')
    
    def change_text(self):
        self.label_widget.text = self.text_input.text.upper()
    
    def save(self, my_textinput, my_texinput1):
        store = JsonStore('hello.json')
        store.put(my_textinput, meaning=my_texinput1)
    
    def start(self):  
        store = JsonStore('hello.json')      
        for i in store.find(word="windows"):
           self.l1.text = str(i[1])
    
    def check(self):
        store = JsonStore('hello.json')
        for i in store.find(word="windows"):
           self.l2.text = str(i[1]) 
        
            


class MyyApp(App):

    theme_cls = ThemeManager()
    title = 'Memo cards'

    def build(self):
        self.theme_cls.theme_style = 'Light'
        return Conteiner()

if __name__ == '__main__':
    MyyApp().run()