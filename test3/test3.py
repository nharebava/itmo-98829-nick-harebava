import socket

import sys

from threading import Thread

from PyQt5 import QtCore, QtGui, QtWidgets


def client():
    main_window.show()
    sock = socket.socket()
    addr = ("127.0.0.1", 8000)

    try:
        sock.connect(addr)
    except ConnectionRefusedError:
        msg = QtWidgets.QMessageBox()
        msg.setText("Сервер отсутствует")
        okButton = msg.addButton("Закрыть программу", QtWidgets.QMessageBox.ApplyRole)
        msg.exec_()
        if msg.clickedButton() == okButton:
            sys.exit()

    ui.chat.append("Подключено к серверу")
    main_window.setWindowTitle("Клиент")
    ui.SendButton.clicked.connect(lambda a: clientsend(ui, sock))
    thread1 = Thread(target=response, args=(sock,))
    thread1.start()
    app.exec_()


def clientsend(ui, sock):
    s = ui.textInput.toPlainText().strip()
    sock.send(s.encode("utf-8"))
    ui.chat.setTextColor(QtGui.QColor(0, 255, 0))
    ui.chat.append(f'клиент: {s}')
    ui.textInput.clear()


def connection():
    sock = socket.socket()
    try:
        sock.bind(("", 8000))
    except OSError:
        errormsg = QtWidgets.QMessageBox()
        errormsg.setText("Сервер уже запущен")
        okButton = errormsg.addButton("Закрыть программу", QtWidgets.QMessageBox.ApplyRole)
        errormsg.exec_()
        if errormsg.clickedButton() == okButton:
            app.exit()
    sock.listen(1)
    global conn
    conn, addr = sock.accept()
    ui.chat.append("Клиент присоединился")
    ui.SendButton.setEnabled(True)
    thread2 = Thread(target=request, args=(conn,))
    thread2.start()


def request(conn):
    try:
        while True:
            request = conn.recv(1024).decode("utf-8")
            ui.chat.setTextColor(QtGui.QColor(255, 0, 0))
            ui.chat.append(f"клиент: {request}")
    except ConnectionResetError:
        conn.close()
        ui.chat.setTextColor(QtGui.QColor(0, 0, 0))
        ui.chat.append("Клиент отсоединился")
        ui.chat.append("Ожидание клиента")
        ui.SendButton.setEnabled(False)


def response(sock):
    try:
        while True:
            response = sock.recv(1024).decode("utf-8")
            ui.chat.setTextColor(QtGui.QColor(255, 0, 0))
            ui.chat.append(f"сервер: {response}")
    except ConnectionResetError:
        sock.close()
        msg = QtWidgets.QMessageBox()
        msg.setText("Сервер отключился")
        okButton = msg.addButton("Закрыть программу", QtWidgets.QMessageBox.ApplyRole)
        msg.exec_()
        if msg.clickedButton() == okButton:
            app.exit()

def server():
    main_window.show()
    ui.SendButton.setEnabled(False)
    ui.chat.append("Ожидание клиента")
    main_window.setWindowTitle("Сервер")
    ui.SendButton.clicked.connect(lambda a: serversend(ui, conn))
    thread1 = Thread(target=connection)
    thread1.start()
    app.exec_()


def serversend(ui, conn):
    s = ui.textInput.toPlainText().strip()
    conn.send(s.encode("utf-8"))
    ui.chat.setTextColor(QtGui.QColor(0, 255, 0))
    ui.chat.append(f'сервер: {s}')
    ui.textInput.clear()


class Ui_MainWindow():
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(400, 600)
        MainWindow.setFixedSize(400, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.textInput = QtWidgets.QTextEdit(self.centralwidget)
        self.textInput.setGeometry(QtCore.QRect(0, 550, 280, 50))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.textInput.setFont(font)
        self.textInput.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textInput.setObjectName("textInput")
        self.SendButton = QtWidgets.QPushButton(self.centralwidget)
        self.SendButton.setGeometry(QtCore.QRect(280, 550, 120, 50))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.SendButton.setFont(font)
        self.SendButton.setObjectName("SendButton")
        self.SendButton.setText("SEND")
        self.chat = QtWidgets.QTextEdit(self.centralwidget)
        self.chat.setGeometry(QtCore.QRect(5, 5, 390, 540))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.chat.setFont(font)
        self.chat.setReadOnly(True)
        self.chat.setObjectName("chat")
        MainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Messenger"))


class QMainWindows(QtWidgets.QMainWindow):
    def closeEvent(self, event):
        msg = QtWidgets.QMessageBox()
        msg.setText("Точно закрыть программу?")
        closeButton = msg.addButton("Да", QtWidgets.QMessageBox.ApplyRole)
        msg.addButton("Нет", QtWidgets.QMessageBox.ApplyRole)
        msg.exec_()
        if msg.clickedButton() == closeButton:
            raise ConnectionResetError
        else:
            event.ignore()


app = QtWidgets.QApplication(sys.argv)
main_window = QMainWindows()

ui = Ui_MainWindow()
ui.setupUi(main_window)
ui.retranslateUi(main_window)

RoleMsg = QtWidgets.QMessageBox()
ServerButton = RoleMsg.addButton("Сервер", QtWidgets.QMessageBox.ApplyRole)
ServerButton.setObjectName("ServerButton")
ClientButton = RoleMsg.addButton("Клиент", QtWidgets.QMessageBox.ApplyRole)
ClientButton.setObjectName("ClientButton")
RoleMsg.setWindowTitle("Роль")
RoleMsg.setText("Выберите роль")
RoleMsg.exec_()
if RoleMsg.clickedButton() == ServerButton:
    server()
else:
    client()
