with open('3610input.txt', 'r') as inp:
    x = float(inp.readline()) 
if x % 1 >= 0.5:
    a = int(x // 1 + 1)
if x % 1 < 0.5:
    a = int(x // 1)
with open('3610output.txt', 'w') as out:
    out.write(str(a))