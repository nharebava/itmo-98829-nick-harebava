from history import hist


def calculator(a, b, oper):
    if oper == "+":
        result = a + b
        hist(str(a),'+', str(b), str(result))
    elif oper == "-":
        result = a - b
        hist(str(a),'-', str(b), str(result))
    elif oper == "*":
        result = a * b
        hist(str(a),'*', str(b), str(result))
    elif oper == "/":
        result = a / b
        hist(str(a),'/', str(b), str(result))
    elif oper == "**":
        result = a ** b
        hist(str(a),'**', str(b), str(result))
    return result
           