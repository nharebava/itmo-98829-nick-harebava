
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

Base = declarative_base()


class Common:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)


class Dishe(Common, Base):
    __tablename__ = "Dishes"
    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)


class Price(Common, Base):
    __tablename__ = "Prices"

    price = sa.Column(sa.Float, sa.CheckConstraint("price>=0"), nullable=False)
    dishe_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(f"{Dishe.__tablename__}.id"),
        nullable=False
    )


class Sale(Common, Base):
    __tablename__ = "Sales"

    dishe_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(f"{Dishe.__tablename__}.id"),
        nullable=False
    )
    amount = sa.Column(sa.Float, nullable=False)


def add(function):
    def wrapper(self, *args, **kwargs):
        record = function(self, *args, **kwargs)
        self._session.add(record)
        self._session.commit()
        return record
    return wrapper


class Position:
    def __init__(self, engine, name, host=None, port=None, username=None, password=None, **params):
        dsn = f"{engine}://"
        if username is not None:
            dsn += username
        if password is not None:
            dsn += f":{password}"
        if username is not None:
            dsn += "@"
        if host is not None:
            dsn += f"{host}"
        if port is not None:
            dsn += f":{port}"
        dsn += f"/{name}"
        if params:
            dsn += "?"
        dsn += "&".join(f"{key}={value}" for key, value in params.items())

        self._engine = sa.create_engine(dsn)
        Base.metadata.create_all(self._engine)
        self._session = sessionmaker(bind=self._engine)()


class WaiterPos(Position):
    def get_dishe(self, name):
        return self._session.query(Dishe).filter(Dishe.name == name).first()

    def get_amount_dishe(self, dishe):
        return self._session.query(func.sum(Sale.amount).label("amount")).filter(
            Sale.dishe_id == dishe.id,
        ).first()[0]

    @add
    def sale(self, name, amount):
        dishe = self.get_dishe(name)
        if dishe is None:
            raise Exception("Has no position with name '%s'" % name)
        has_amount = self.get_amount_dishe(dishe)
        if amount > has_amount:
            raise Exception("Has no %f amount for sell '%s'. Current amount: %f" % (
                amount, name, has_amount,
            ))
        return Sale(dishe_id=dishe.id, amount=-amount)


class GuestPos(Position):
    def get_dishe(self, name):
        return self._session.query(Dishe).filter(Dishe.name == name).first()

    @add
    def income(self, name, amount,):
        dishe = self.get_dishe(name)
        if dishe is None:
            raise Exception("Only Cheff can add new dish")
        return Sale(dish_id=dishe.id, amount=amount)


class CheffPos(WaiterPos, GuestPos):
    @add
    def cheffin(self, name, amount, price):
        dishe = Dishe(name=name)
        self._session.add(dishe)
        self._session.commit()
        price = Price(price=price, dishe_id=dishe.id)
        self._session.add(price)
        self._session.commit()

        return Sale(dishe_id=dishe.id, amount=amount)


cheff = CheffPos(engine="sqlite", name="restaurant.db")
waiter = WaiterPos(engine="sqlite", name="restaurant.db")
guest = GuestPos(engine="sqlite", name="restaurant.db")
cheff.cheffin(name="Borsch", amount=100, price=1000)
cheff.cheffin(name="Olivie", amount=200, price=2000)
cheff.cheffin(name="Beefstroganoff", amount=300, price=3000)
cheff.sale(name="Beefstroganoff", amount=100)
 