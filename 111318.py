# Какой тип данных возвращает input()? Что делает функция str()?
str1, str2 = str(input()), str(input())
# Можно использовать оператор проверки вхождения in
# И однострочный if
if str2.find(str1) == -1:
    print('NO')
else:
    print('YES')
